**No time to learn OpenAPI :(**
# `/api/`
## GET
_HTTP 200 OK_
```json
{
  "versions": ["v1"]
}
```

#  `/api/v1`
## GET
_HTTP 200 OK_

# `/api/v1/user`
## GET
**Desc:**
Returns informations about logged in user. 

_HTTP 200 OK_
- status - `"online"`
```json
{
  "name": "string",
  "status": "string"
}
```

_HTTP 403 Forbidden_

# `/api/v1/user/connections`
## GET
**Desc:**
Returns connections/friends of logged in user.

_HTTP 200 OK_
- status - `"online/offline"`
```json
[
  {
    "username": "string",
    "status": "string",
    "since": "dateTime"
  },
    ...
]
```
_HTTP 403 Forbidden_

# `/api/v1/user/connections/${username}`
## GET
**Desc:**
Returns info about logged in user's particular connection.

_HTTP 200 OK_
- status - `"online/offline"`
```json
{
    "username": "string",
    "status": "string"
}
```

_HTTP 404 Not Found_

_HTTP 403 Forbidden_

# `/api/v1/user/pending/invitations`
## GET
**Desc:**
Returns info of logged in user's connection invitations sent/received.

_HTTP 200 OK_
- type - `"sent/received"`
```json
[
  {
    "username": "string",
    "type": "string",
    "since": "dateTime"
  },
    ...
]
```

_HTTP 403 Forbidden_