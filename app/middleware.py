import string

from jwt import InvalidSignatureError
from fastapi import Request, Response
from sqlalchemy.sql.functions import now
from starlette.responses import JSONResponse
from app.utils import VerifyToken, is_authenticated
from app.schemas import users as users_schema

class AuthMiddleware:
    def __init__(
            self):
        pass

    async def __call__(self, request: Request, call_next):
        try:
            token = request.headers.get("Authorization")
            if not token:
                raise InvalidSignatureError(400)
            auth_result = VerifyToken(token.replace("Bearer ", "")).verify()
            if is_authenticated(auth_result):
                request.state.auth = auth_result
                response = await call_next(request)
                return response
            else:
                return Response(status_code=403, content="Forbidden")
        except InvalidSignatureError as er:
            return JSONResponse(status_code=401, content="Unauthorized")
