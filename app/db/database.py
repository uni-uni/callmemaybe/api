import os

from sqlalchemy import create_engine
from sqlalchemy.engine import URL
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker


db_username = os.getenv("DB_USERNAME")
db_password = os.getenv("DB_PASSWORD")
db_host = os.getenv("DB_HOST")
db_port = os.getenv("DB_PORT")
db_name = os.getenv("DB_NAME")


url_object = URL.create(
    "postgresql",
    username=db_username,
    password=db_password,  # plain (unescaped) text
    host=db_host,
    port=db_port,
    database=db_name,
)

engine = create_engine(
    url_object
)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

# inherit from this class to create each of the database models or classes
Base = declarative_base()
