from sqlalchemy import null
from sqlalchemy.orm import Session

from sqlalchemy.sql.functions import now

from ..models.model import users, groups, connections, calls, messages, invitations, user_groups

from .groups import create_group
from .users import get_user

def create_invitation(db: Session, sender_email: str, receiver_email: str):
    # check if invitaition is sent
    sender = db.query(users.users_id).filter(users.email == sender_email).first()
    receiver = db.query(users.users_id).filter(users.email == receiver_email).first()
    invitation = invitations(sending_date=now(), sender=sender['users_id'], receiver=receiver['users_id'],
                             invitation_status="p")
    db.add(invitation)
    db.commit()
    db.refresh(invitation)
    return invitation


def get_invitations(email: str, db: Session):
    invitation_list = db.query(invitations).join(users, users.users_id == invitations.sender).filter(
        users.email == email).all()
    invitation_list2 = db.query(invitations).join(users, users.users_id == invitations.receiver).filter(
        users.email == email).all()
    return invitation_list + invitation_list2


def change_status(email: str, invitation_id: int, status: str, db: Session):
    if status not in ["accepted", "rejected"]:
        return null
    invitation = db.query(invitations).filter(invitations.invitation_id == invitation_id).first()
    if invitation.invitation_status not in ["accepted", "rejected", "pending"]:
        return null
    invitation.invitation_status = status
    db.commit()
    db.refresh(invitation)
    if status == "accepted":
        connection = connections(connection_status=status, established_date=now(), sender=invitation.sender,
                                 receiver=invitation.receiver, invitation=invitation.invitation_id)
        connection_2 = connections(connection_status=status, established_date=now(), sender=invitation.receiver,
                                   receiver=invitation.sender, invitation=invitation.invitation_id)
        db.add(connection)
        db.commit()
        db.refresh(connection)
        db.add(connection_2)
        db.commit()
        db.refresh(connection_2)

        receiver_email = get_user(db, invitation.receiver).email
        create_group(email, "private", [receiver_email], db)

    return connection

