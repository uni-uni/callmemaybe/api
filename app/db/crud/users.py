from sqlalchemy.orm import Session
from ..models.model import users, groups, connections, calls, messages, invitations, user_groups
from sqlalchemy.sql.functions import now
from app.schemas import users as users_schema, groups as groups_schema, connections as connections_schema


def create_user( email: str, username: str, db: Session):
    db_user = users(username=username, email=email, creation_date=now())
    db.add(db_user)
    db.commit()
    db.refresh(db_user)
    return db_user


def get_user(db: Session, user_id: int):
    return db.query(users).filter(users.users_id == user_id).first()


def get_user_by_email(db: Session, email: str):
    return db.query(users).filter(users.email == email).first()


def get_users(db: Session):
    return db.query(users).all()

