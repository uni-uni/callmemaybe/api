from typing import List

from sqlalchemy import null
from sqlalchemy.orm import Session
from ..models.model import users, groups, connections, calls, messages, invitations, user_groups
from .users import get_user_by_email
from ...schemas import users as schemas_users
from .connections import get_connections, get_connections_emails


# jesli user nie ma stworzonej grupy
def create_group(email: str, group_name: str, users_email_to_add: List[str], db: Session):
    if len(users_email_to_add) < 1:
        return null
    sender = db.query(users).filter(users.email == email).first()
    user_connections = get_connections_emails(db, sender)
    user_connections_emails = []
    for uc in user_connections:
        user_connections_emails.append(uc.email)
    for email_to_add in users_email_to_add:
        if email_to_add not in user_connections_emails or email_to_add == sender.email:
            return null
    new_group = groups(group_name=group_name)
    db.add(new_group)
    db.commit()
    db.refresh(new_group)
    user_group = user_groups(user=sender.users_id, group=new_group.group_id)
    db.add(user_group)
    db.commit()
    db.refresh(user_group)
    for email in users_email_to_add:
        add_user_to_group(new_group.group_id, email, db)


def get_group_for_user(email: str, db: Session):
    user = get_user_by_email(db, email)
    users_group = db.query(user_groups).filter(user.users_id == user_groups.user).all()
    result = []
    if users_group:
        for user_list in users_group:
            # cell=get_last_messages(user.email, user_list.group, db)
            # last_message.append(cell)
            temp = db.query(user_groups.user, groups.group_id, groups.group_name).join(groups, groups.group_id == user_groups.group).filter(user_list.group == user_groups.group).all()
            result.append(temp)
        return result
    else:
        return "Ni jezeź w żodny grubie heh"

def add_user_to_group(group_id: int, receiver_email: str, db: Session):
    receiver = db.query(users.users_id).filter(users.email == receiver_email).first()
    user_group_added = user_groups(user=receiver.users_id, group=group_id)
    db.add(user_group_added)
    db.commit()
    db.refresh(user_group_added)
    return user_group_added

