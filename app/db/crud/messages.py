from sqlalchemy import true, false
from sqlalchemy.orm import Session

from sqlalchemy.sql.functions import now
from starlette.responses import Response

from ..models.model import users, groups, connections, calls, messages, invitations, user_groups

from app.schemas import users as users_schema, groups as groups_schema, connections as connections_schema
from .users import get_user_by_email


def user_belong_to_group(user_email: str, group_id: int, db: Session):
    user_to_check = get_user_by_email(db, user_email)
    user_groups_return = db.query(user_groups.group).filter(user_groups.user == user_to_check.users_id and user_groups.group == group_id).all()
    user_belongs = False
    for g in user_groups_return:
        if g['group'] == group_id:
            user_belongs = True
    if user_to_check and user_belongs:
        return True
    else:
        return False


def get_messages(user_email: str, group_id: int, db: Session):
    if user_belong_to_group(user_email, group_id, db):
        return db.query(users.username, users.email, messages).join(users, users.users_id == messages.sender).filter(messages.group == group_id).order_by(messages.sending_date).all()
    return Response(status_code=403, content="Forbidden")


def send_messages(user_email: str, group_id: int, context: str, db: Session):
    if user_belong_to_group(user_email, group_id, db):
        message = messages(sending_date=now(), content=context, sender=get_user_by_email(db, user_email).users_id, group=group_id, message_status="s")
        db.add(message)
        db.commit()
        db.refresh(message)
        return message
    else:
        "Nie należysz do tej grupy"

def get_last_messages(user_email: str, group_id: int, db: Session):
    if user_belong_to_group(user_email, group_id, db):
        return db.query(users.users_id, users.username, users.email, messages).join(users, users.users_id == messages.sender).filter(messages.group == group_id).order_by(messages.sending_date.desc()).first()
    return "Nie należysz do tej grupy!"

