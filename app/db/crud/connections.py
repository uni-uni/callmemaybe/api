from sqlalchemy.orm import Session

from sqlalchemy.sql.functions import now

from ..models.model import users, groups, connections, calls, messages, invitations, user_groups

from app.schemas import users as users_schema, groups as groups_schema, connections as connections_schema


def get_connections(db: Session, user: users_schema.User):
    connection = db.query(users).join(connections, users.users_id == connections.receiver).filter(
        connections.sender == user.users_id).all()
    return connection


def get_connections_emails(db: Session, user: users_schema.User):
    connection = db.query(users.email).join(connections, users.users_id == connections.receiver).filter(
        connections.sender == user.users_id).all()
    return connection