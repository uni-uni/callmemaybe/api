from sqlalchemy.ext.automap import automap_base
from sqlalchemy.orm import Session
from app.db import database

Base = automap_base()

# reflect the tables
Base.prepare(autoload_with=database.engine)

# mapped classes are now created with names by default
# matching that of the table name.
users = Base.classes.users
groups = Base.classes.groups
calls = Base.classes.calls
connections = Base.classes.connections
invitations = Base.classes.invitations
messages = Base.classes.messages
user_groups = Base.classes.user_groups

session = Session(database.engine)
