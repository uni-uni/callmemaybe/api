from datetime import datetime

from pydantic import BaseModel


class Call(BaseModel):
    call_id: int
    started_date: datetime
    ended_date: datetime
    call_status: str


    class Config:
        orm_mode = True
