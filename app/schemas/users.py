from datetime import datetime
from typing import List, Optional

from pydantic import BaseModel

from .calls import Call
from .connections import Connection
from .groups import Group
from .invitations import Invitation
from .messages import Message


class User(BaseModel):
    users_id: Optional[int]
    username: str
    email: Optional[str]
    creation_date: datetime
    user_type: Optional[str]
    user_status: Optional[str]
    icon_url: Optional[str]
    messages: Optional[List[Message]]
    groups: Optional[List[Group]]
    calls: Optional[List[Call]]
    invitations: Optional[List[Invitation]]
    connections: Optional[List[Connection]]

    class Config:
        orm_mode = True
