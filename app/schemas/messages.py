from datetime import datetime
from typing import Optional

from pydantic import BaseModel


class Message(BaseModel):
    messages_id: Optional[int]
    sending_date: Optional[datetime]
    context: str
    message_status: Optional[str]

    class Config:
        orm_mode = True
