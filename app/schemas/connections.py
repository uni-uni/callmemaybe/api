from datetime import datetime
from typing import Optional, List

from pydantic import BaseModel

from .invitations import Invitation


class Connection(BaseModel):
    connections_id: int
    connection_status: str
    established_date: datetime
    sender: int
    receiver: int
    invitation: Optional[List[Invitation]]


    class Config:
        orm_mode = True
