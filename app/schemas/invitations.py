from datetime import datetime

from pydantic import BaseModel


class Invitation(BaseModel):
    invitation_id: int
    sending_date: datetime
    sender: int
    receiver: int
    invitation_status: str

    class Config:
        orm_mode = True
