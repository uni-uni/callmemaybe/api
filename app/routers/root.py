from fastapi import APIRouter, Request, Response, status

# from app.middleware import AuthorizedRequest

router = APIRouter()

@router.get("/")
async def root():
    return {"message": "Hello World"}


@router.get("/api/private")
async def say_hello(request: Request, response: Response):
    """A valid access token is required to access this route"""

    return request.state.auth
