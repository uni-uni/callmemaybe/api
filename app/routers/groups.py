from typing import List
from urllib import response

from fastapi import APIRouter, Depends, HTTPException, Request, Body
from sqlalchemy.orm import Session

from app.db.crud import users as crud_users, connections as crud_connections, invitations as crud_invitations, groups as crud_groups

from app.utils import get_db
from app.schemas import users, groups, connections, invitations, messages, calls

router = APIRouter(prefix="/api/v1/groups")


@router.get("/all/", response_model=None)
def read_group(request: Request, db: Session = Depends(get_db)):
    group_list = crud_groups.get_group_for_user(request.state.auth['useremail'], db)
    return group_list


@router.post("/add_user", response_model=None)
def add_to_group(request: Request, email: str = Body(embed=True), group_name: str = Body(embed=True), db: Session = Depends(get_db)):
    user_to_add = crud_groups.add_user_to_group(request.state.auth['useremail'], email, group_name, db)
    return user_to_add
@router.post("/create_group", response_model=None)
def add_to_group(request: Request, email: List[str]= Body(embed=True), group_name: str = Body(embed=True), db: Session = Depends(get_db)):
    user_to_add = crud_groups.create_group(request.state.auth['useremail'],  group_name, email, db)
    return user_to_add

