from typing import List
from urllib import response

from fastapi import APIRouter, Depends, HTTPException, Request, Body
from sqlalchemy.orm import Session

from app.db.crud import users as crud_users, connections as crud_connections, invitations as crud_invitations

from app.utils import get_db
from app.schemas import users, groups, connections, invitations, messages, calls

router = APIRouter(prefix="/api/v1/users/invitations")


@router.post("/send", response_model=None)
def create_invitations(request: Request, user_email_to_add: str = Body(embed=True), db: Session = Depends(get_db)):
    if not user_email_to_add:
        raise HTTPException(status_code=400, detail="User with this email doesn't exist")
    return crud_invitations.create_invitation(db, request.state.auth['useremail'], user_email_to_add)


@router.get("/all", response_model=None)
def read_invitations(request: Request, db: Session = Depends(get_db)):
    invitations_list = crud_invitations.get_invitations(request.state.auth['useremail'], db)
    return invitations_list


@router.post("/change_status", response_model=None)
def change_status(request: Request, invitation_id: int= Body(embed=True), status: str= Body(embed=True), db: Session = Depends(get_db)):
    return crud_invitations.change_status(request.state.auth['useremail'], invitation_id, status, db)


