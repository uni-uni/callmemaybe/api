from typing import List
from urllib import response

from fastapi import APIRouter, Depends, HTTPException, Request, Body
from sqlalchemy.orm import Session

from app.db.crud import users as crud_users, connections as crud_connections

from app.utils import get_db
from app.schemas import users, groups, connections, invitations, messages, calls

router = APIRouter(prefix="/api/v1/users")


@router.post("/register", response_model=users.User)
def create_user(request: Request, db: Session = Depends(get_db)):
    db_user = crud_users.get_user_by_email(db, email=request.state.auth['useremail'])
    if db_user:
        raise HTTPException(status_code=409, detail="Email already registered")
    return crud_users.create_user(request.state.auth['useremail'], request.state.auth['username'], db=db)


@router.get("/all", response_model=None)
def read_users(db: Session = Depends(get_db)):
    users_list = crud_users.get_users(db)
    return users_list


@router.get("/detail", response_model=None)
def read_user(request: Request, db: Session = Depends(get_db)):
    user = crud_users.get_user_by_email(db, request.state.auth['useremail'])
    return user


@router.get("/connections", response_model=None)
def read_users(request: Request, db: Session = Depends(get_db)):
    user = crud_users.get_user_by_email(db, request.state.auth['useremail'])
    if user:
        connection_list = crud_connections.get_connections(db, user)
    return connection_list


