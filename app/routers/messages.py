import json
from typing import Union, List
import jwt
from datetime import datetime

import websockets
from fastapi import (
    Cookie,
    Depends,
    FastAPI,
    Query,
    WebSocket,
    WebSocketException,
    status,
    APIRouter,
    Request, Body
)
from fastapi.responses import HTMLResponse
from jwt import InvalidSignatureError
from sqlalchemy.orm import Session
from sqlalchemy.sql.functions import now
from starlette.websockets import WebSocketState, WebSocketDisconnect

from app.db.crud import messages as crud_messages
from app.db.crud.messages import user_belong_to_group

from app.utils import get_db, VerifyToken, is_authenticated

router = APIRouter(prefix="/api/v1/users/messages")


class SocketManager:
    def __init__(self):
        self.active_connections: List[(WebSocket, str, int)] = []

    def connect(self, websocket: WebSocket, user: str, group_id: int):
        # await websocket.accept()
        if (websocket, user, group_id) in self.active_connections:
            return
        self.active_connections.append((websocket, user, group_id))

    def disconnect(self, websocket: WebSocket, user: str, group_id):
        self.active_connections.remove((websocket, user, group_id))

    async def send_data(self, data, group_id):
        active_connections = self.active_connections.copy()
        for con in active_connections:
            if con[0].client_state is WebSocketState.DISCONNECTED:
                self.disconnect(con[0], con[1], con[2])
                continue
            if group_id == con[2]:
                await con[0].send_json(data)


socket_manager = SocketManager()


@router.get("/{group_id}", response_model=None)
def get_messages(request: Request, group_id: int, db: Session = Depends(get_db)):
    return crud_messages.get_messages(request.state.auth['useremail'], group_id, db)


@router.post("/{group_id}", response_model=None)
def send_messages(request: Request, group_id: int, content: str = Body(embed=True), db: Session = Depends(get_db)):
    return crud_messages.send_messages(request.state.auth['useremail'], group_id, content, db)


@router.websocket("/ws/{group_id}")
async def ws(websocket: WebSocket, group_id: int, db: Session = Depends(get_db)):
    auth_result = None
    try:
        await websocket.accept()
        while True:
            data = await websocket.receive_json()
            auth_result = VerifyToken(data.get("token")).verify()
            if not is_authenticated(auth_result) or not user_belong_to_group(auth_result['useremail'], group_id, db):
                raise InvalidSignatureError(403)
            socket_manager.connect(websocket, auth_result['useremail'], group_id)

            if data.get("text") and data.get("text") is not " ":
                response = {
                    "email": auth_result['useremail'],
                    "messages": {
                        "content": data.get("text"),
                        "sending_date": datetime.now().strftime("%Y-%m-%dT%H:%M:%S")
                    }
                }
                crud_messages.send_messages(auth_result['useremail'], group_id, data.get("text"), db)
                await socket_manager.send_data(response, group_id)
    except InvalidSignatureError as e:
        await websocket.send_json({"status": "InvalidSignatureError"})
    except Exception as e:
        print(e)
