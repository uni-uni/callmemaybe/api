import os

import uvicorn
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from starlette.middleware.base import BaseHTTPMiddleware

from app.middleware import AuthMiddleware

from app.routers import users,groups, invitations, root, messages

app = FastAPI()
# Base.metadata.create_all(engine)

auth_middleware = AuthMiddleware()
app.add_middleware(BaseHTTPMiddleware, dispatch=auth_middleware)

origins = [
    "http://localhost",
    "http://localhost:3000",
    "https://callmemaybe.onrender.com",
    "https://callmemaybe-api.onrender.com"
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

# utils.get_db()
app.include_router(users.router)
app.include_router(root.router)
app.include_router(groups.router)
app.include_router(invitations.router)
app.include_router(messages.router)

if __name__ == "__main__":
    uvicorn.run(app, host="127.0.0.1", port=8000)
