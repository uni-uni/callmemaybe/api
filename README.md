### This is a python fastapi backend for callmemaybe

Recommended version of python:
3.11

In order to run the backend you need to:
* Setup the https://gitlab.com/uni-uni/callmemaybe/maybase and export env variables:
```
     $ export DB_HOST={host}
     $ export DB_NAME={dbname}
     $ export DB_PASSWORD={pass}
     $ export DB_PORT={port}
     $ export DB_USERNAME={username}
```
* Provide `.config` file with Your Auth0 API config.
Instructions for creating Auth0 app and api:
  - https://auth0.com/docs/get-started/auth0-overview/create-applications
  - https://auth0.com/docs/get-started/auth0-overview/set-up-apis
```
[AUTH0]
DOMAIN = {your-domain}.us.auth0.com
API_AUDIENCE = {your-audience}
ALGORITHMS = RS256
ISSUER = https://{your-domain}.us.auth0.com/
```
* Install requirements and run the app
```
   $ pip install -r requirements.txt
   $ uvicorn app.main:app --reload
```

In case of troubles, see: https://fastapi.tiangolo.com/tutorial/